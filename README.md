## Lockbox

Super, super simple version of [Vault](https://www.hashicorp.com/products/vault/) that doesn't require any setup.

Basically the idea is to make static .profile files pointless and hide things slightly better.

### Installation

`npm install -g @qnzl/lockbox`

### Usage

Keys can be folders (eg: `<top>/<lower>/<more>.../<key>`).

To save:
```bash
lockbox <key> <value>
```

To load:
```bash
lockbox <key>
```

### Config

Unless `lockbox --config` is used to change config file location, `lockbox.conf` will be in $HOME

The available config options are:

- `location` - Absolute path of where you want your Lockbox files stored
- `key` - Absolute path of your RSA key
- `gitRemote` - git-compatiable remote location
