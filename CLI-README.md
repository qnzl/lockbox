### lockbox(1)

#### Abstract

All other commands prepended with `lockbox-` execute as a child of this command

#### Usage

```sh
Usage: lockbox [options] [command]

  Commands:
helpDisplay help
list, l  List all keys available
version  Display version

  Options:
-c, --config [value]  Config for Lockbox (defaults to "/home/maddie/.lockbox.conf")
-h, --helpOutput usage information
-v, --versionOutput the version number


```

#### Options
| Flag(s) | Description | Type | Default |
|--|--|--|--|



